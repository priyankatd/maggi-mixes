<section>
	<div class="mixes-container">
		<div class="mixes-recipes-container clearfix">
			<div class="col-md-6 text-center">
				<h1 class="mixes-heading"></h1>
				<p class="mixes-description"></p>
				<div class="recipe-description-container">
					<h3 class="recipe-heading"></h3>
					<p class="recipe-description"></p>
					<button class="red-btn know-more"></button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="recipe-image">
					<img src="assets/images/bechamel-mix-large.png"/>
				</div>
			</div>
		</div>
		<div class="mixes-recipes row">

		</div>
	</div>
	<div class="mixes-recipes-video">
		<div class="recipe-video-description-container text-center">
			<h2 class="recipe-video-heading"></h2>
			<p class="recipe-video-description"></p>
		</div>
		<div class="recipes-video-section">
		</div>
		<div class="load-more-section">
			<button class="red-btn load-more"></button>
		</div>
	</div>
</section>