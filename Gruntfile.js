module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // concat: {
    //   options: {
    //     separator: ';'
    //   },
    //    dist: {
    //     src: ['js/*.js'],
    //     dest: 'dist/main.js'
    //   }
    // },
    sass: {
      dist: {
        files: {
          'css/custom/main.css': 'sass/*.scss'
        }
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          // cwd: 'css/custom/',
          src: ['css/custom/*.css'],
          dest: 'assets/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      // options: {
      //   mangle: {
      //     except: ['jQuery']
      //   }
      // },
      my_target: {
        files: {
          'assets/js/vendor.min.js': ['js/vendor/*.js'],
          'assets/js/custom.min.js': ['js/custom/*.js']
        }
      }
    },
    watch: {
      sass: {
        files: 'sass/*.scss',
        tasks: ['sass']
      },
      cssmin: {
        files: 'css/custom/*.css',
        tasks: ['cssmin']
      },
      uglify: {
        files: 'js/*/*.js',
        tasks: ['uglify']
      }
    },
    
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['sass']);

};